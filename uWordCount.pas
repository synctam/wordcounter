unit uWordCount;

interface

uses
  Winapi.Windows, System.SysUtils, System.UITypes, Vcl.Dialogs, Classes, mPlugin;

type
  TWordCount = class
  private
    class function GetWordCount(text: string): integer;
  public
    class function ShowWord(hWnd: hWnd): string;
  end;

implementation

const
  // 区切り文字：単語の区切りを示す文字
  // 独自方式
  cc1 = #1#2#3#4#5#6#7#8#9#10#11#12#13#14#15#16#17#18#19;
  cc2 = #20#21#22#23#24#25#26#27#28#29;
  cc3 = #30#31#32#33#34#35#36#37#38#39;
  cc4 = #40#41#42#43#44#45#46#47;
  cc5 = #58#59#60#61#62#63#64;
  cc6 = #91#92#93#94#95#96;
  cc7 = #123#124#125#126;
  // MS Word, wc 方式
  wd1 = #1#2#3#4#5#6#7#8#9#10#11#12#13#14#15#16#17#18#19;
  wd3 = #32;
  // Notepad++方式
  np1 = #1#2#3#4#5#6#7#8#9#10#11#12#13#14#15#16#17#18#19;
  np2 = #20#21#22#23#24#25#26#27#28#29;
  np3 = #30#31#32#33#34#36#37#38#39;
  np4 = #40#41#42#43#44#45#46#47;
  np5 = #58#59#60#61#62#63#64;
  np6 = #91#92#93#94#96;
  np7 = #123#124#125#126;

  ctrlChars_STD = cc1 + cc2 + cc3 + cc4 + cc5 + cc6 + cc7;
  ctrlChars_WC = wd1 + wd3;
  ctrlChars_NP = np1 + np2 + np3 + np4 + np5 + np6 + np7;

  // Notepad++方式を使用する。
  split_code = ctrlChars_NP;

  //
  // GetWordCount
  // 指定されたテキストのワード数を返す。
  // パラメータ
  // text: テキスト
  //
class function TWordCount.GetWordCount(text: string): integer;
begin
  try
    result := 0;
    // 区切り文字で分割しワード数を算出する。
    for var word in text.Split(split_code.ToCharArray()) do
    begin
      if not word.isEmpty then
      begin
        inc(result);
      end;
    end;
  except
    on E: Exception do
    begin
      result := -1;
    end;
  end;
end;

// CountWord
// 現在開いているテキストのワード数などを表示する。
// パラメタ
// hwnd: ウィンドウのハンドル
//
class function TWordCount.ShowWord(hWnd: hWnd): string;
var
  lineInfo: TGetLineInfo;
  LBuf: array of Char;
  charCount: integer;
const
  NL: string = #13#10;
begin
  try
    charCount := 0;
    // 行数の取得
    var lines := Editor_GetLines(hWnd, POS_LOGICAL);
    var linesWithOutEmpty: integer := 0;
    var linesStr := FormatFloat('#,##0', double(lines));
    var words: integer := 0;
    for var i := 0 to lines - 1 do
    begin
      // ライン情報の初期化
      lineInfo.cch := 0;
      lineInfo.byteCrLf := 0;
      lineInfo.flags := FLAG_LOGICAL;
      lineInfo.yLine := i;
      // ライン長の取得
      var lineSize := Editor_GetLine(hWnd, @lineInfo, nil);
      if lineSize > 1 then
      begin
        // 空行以外の行をカウントする。
        inc(linesWithOutEmpty);
      end;
      // テキストバッファ長の確保
      SetLength(LBuf, lineSize);
      // 取得するテキスト長を設定
      lineInfo.cch := lineSize;
      // バッファにテキストを取り込む
      Editor_GetLine(hWnd, @lineInfo, PChar(LBuf));
      // 文字数算出
      charCount := charCount + Length(PChar(LBuf));
      // ワード数を算出
      var ans := GetWordCount(PChar(LBuf));
      if ans > 0 then
      begin
        words := words + ans;
      end;
    end;

    var wordsStr := FormatFloat('#,##0', double(words));
    var charCountStr := FormatFloat('#,##0', double(charCount));
    var linesWithoutEmptyStr := FormatFloat('#,##0', double(linesWithOutEmpty));

    // メッセージ出力
    var msg := Format('Characters(without line endings): %s%sWords: %s%sLines: %s%sLines(without empty): %s', [charCountStr, NL, wordsStr, NL, linesStr, NL, linesWithoutEmptyStr]);
    MessageDlg(msg, mtInformation, [mbOk], 0);
  except
    on E: Exception do
    begin
      var msg := Format('システムエラー：%s', [E.Message]);
      MessageDlg(msg, mtError, [mbYes], 0);
    end;
  end;
end;

end.

